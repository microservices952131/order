package com.order.order.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;

import java.util.List;

public class Order {

    private String id;
    private List<String> items;

    @JsonCreator
    public Order(String id, List<String> items) {
        this.id = id;
        this.items = items;
    }

    @JsonGetter
    public String getId() {
        return id;
    }

    @JsonGetter
    public List<String> getItems() {
        return items;
    }
}
