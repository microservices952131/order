package com.order.order.controller;


import com.order.order.models.Order;
import org.apache.catalina.connector.Response;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private static final Map<String, Order> orderMap = new ConcurrentHashMap<>();

    static {
        orderMap.put("d92ad4cd-8bcc-4a74-b882-37ed0a87344f", new Order("d92ad4cd-8bcc-4a74-b882-37ed0a87344f", List.of("Gigabyte motherboard", "I7 Processor", "HyperX Fury Ram - 16gb")));
        orderMap.put("e92ad4cd-8bcc-4a74-b882-37ed0a87344f", new Order("e92ad4cd-8bcc-4a74-b882-37ed0a87344f", List.of("Lipstick", "Mascara", "Eyeliner")));
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Order> getOrders() {
        return orderMap.values();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable(value = "id") String id) {
        if (orderMap.containsKey(id)) {
            return ResponseEntity.ok(orderMap.get(id));
        }
        return ResponseEntity.notFound().build();
    }
}
