# Stage 1: Build the Spring Boot application
FROM gradle:8.2.1-jdk17 AS build
MAINTAINER vikas
WORKDIR /usr/src/order

# Copy the build files and source code to the container
COPY build.gradle .
COPY settings.gradle .
COPY gradlew .
COPY gradle ./gradle
COPY src ./src
RUN chmod +x gradlew

# Build the application
RUN ./gradlew build --no-daemon

# Stage 2: Create the runtime image
FROM openjdk:17
MAINTAINER vikas
WORKDIR /usr/src/order

# Copy the built JAR from the previous stage to the container
COPY --from=build /usr/src/order/build/libs/order-0.0.1-SNAPSHOT.jar ./app.jar

# Command to run the Spring Boot application
ENTRYPOINT ["java", "-jar", "app.jar"]
